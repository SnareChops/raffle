const $button = document.querySelector('button');
const $textarea = document.querySelector('textarea');
const $title = document.querySelector('#raffle-title');
const $area = document.querySelector('#raffle-area');

$button.addEventListener('click', async () => {
  $textarea.rows = 3;
  $button.style.display = 'none';
  $title.innerHTML = 'Finding winner...';
  const [uniques, entries] = parse();
  await animate(uniques);
  $title.innerHTML = 'Congratulations!!';
  $area.innerHTML = `<winner>${roll(entries)}</winner>`;
  $button.style.display = 'block';
  $button.innerHTML = 'Reroll';
  $button.classList.add('soft');
});

function parse() {
  const str = $textarea.value;
  if (!str) {
    return '';
  }

  const entries = str
    .split('\n')
    .map(entry => {
      const [name, tickets] = entry.split(' ');
      return { name, tickets };
    })
    .filter(x => !!x.name);

  return [
    entries.map(x => x.name),
    entries.reduce((result, entry) => {
      const times = Number(entry.tickets);
      for (let i = 0; i < times; i++) {
        result.push(entry.name);
      }
      return result;
    }, [])
  ];
}

function roll(names) {
  return names[Math.floor(Math.random() * names.length)];
}

async function animate(names) {
  let interval;
  await new Promise(resolve => {
    let idx = 0;
    interval = setInterval(() => {
      if (++idx === names.length) {
        idx = 0;
      }
      cycleName(names, idx);
    }, 100);
    setTimeout(resolve, 7000);
  });
  clearInterval(interval);
}

function cycleName(names, idx) {
  $area.innerHTML = `<p class="center searching">${names[idx]}</p>`;
}
